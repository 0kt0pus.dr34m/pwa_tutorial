import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

// The root id comes from index.html file in public
ReactDOM.render(<App/>, document.getElementById('root'));