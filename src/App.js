// This is a default import. Which means the assigned name (React) can be anything
// the name doesnt matter
// Here we do both default and name imports (React and useState hook)
import React, {useState} from 'react';
// this is a named import {}, so you cant use any name but the exported name
import {fetchWeather} from './api/fetchWeather';
// We put all css of App in one file and import it
import './App.css';

const App = () => {
    const [query, setQuery] = useState('');
    // weather data state store (init with an empty obj)
    const [weather, setWeather] = useState({});
    // Search is a function that fetches the data when enter is pressed
    const search = async (e) => {
        if (e.key === 'Enter') {
            const data = await fetchWeather(query)
            console.log(data);
            // now we store the recieved weather data
            setWeather(data); // this makes the weather var (see declaration accessible to whole app through state)
            // reset the query state
            setQuery('');
        }
    };
    
    return(
        <div className="main-container">
            {/*The value input is the query and onClick it take the value given by query
            and call setQuery to set the value*/}
            <input
                type="text"
                className="search"
                placeholder="Search...."
                value={query}
                onChange={(e) => setQuery(e.target.value)}
                onKeyPress={search}
            />
            {/**Now we check if weather exists, if so return a component to show the weather 
             * The weather.main and weather.name corresponds to responded json keys
            */}
            {weather.main && (
                <div className="city">
                    <h2 className="city-name">
                        <span>{weather.name}</span>
                        {/** <sup> is like a super script*/}
                        <sup>{weather.sys.country}</sup>
                        {/** Show the temperature */}
                        <div className="city-temp">
                            {Math.round(weather.main.temp)}
                            <sup>&deg; C</sup>
                        </div>
                        <div className="info">
                            <img className="city-icon" src={`https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`} alt={weather.weather[0].description}/>
                            <p>{weather.weather[0].description}</p>
                        </div>
                    </h2>
                </div>
            )}
        </div>
    );
};
// We can use axios to get wether data from an api
export default App;