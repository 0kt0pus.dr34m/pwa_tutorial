// The cache is the browser storage where it stores the images once downloaded, so no need to 
// download again, because we can take it from the cache
const CACHE_NAME = "version-1";
const urlsToCache = ["index.html", "offline.html"]
// Event for installation of Serive worker
// self: service worker itself
const self = this
self.addEventListener('install', (event) => {
    // open the cache and add the urls
    event.waitUntil(
        // open the cache, this returns a promise
        caches.open(CACHE_NAME)
            // callback of the promise
            .then((cache) => {
                console.log("Cache opened");
                // Add the urls to the opened cache
                return cache.addAll(urlsToCache);
            })
            // You can see the cached files in Application -> Cache storage in dev console
    );

});
// SW listen for requests
self.addEventListener('fetch', (event) => {
    event.respondWith(
        // returns a promise by matching the request
        // requests are like api call image saving
        caches.match(event.request)
            // then fetch the requested caches
            .then(() => {
                // return a promise of all the fetched requests
                // keep matching the request and fetch them
                // this allows to maintatin realtime connection
                // when internet is available
                return fetch(event.request)
                            // when no connection catch with call back
                            // to open the offline page
                            .catch(() => caches.match('offline.html'));
                            // set network -> offline to see the offline page
            })
    );
});
// Activate the SW
self.addEventListener('activate', (event) => {
    const cacheWhitelist = [];
    // white listed caches, because others we delete (remove old caches)
    cacheWhitelist.push(CACHE_NAME);
    // wait till we get the cache keys from the event
    event.waitUntil(
        // returns a promise of keys and return a promise list
        // that does not contain caches other than the whitelisted onces
        // Promise.all() runs the promise array inside it
        caches.keys().then((cacheNames) => Promise.all(
            cacheNames.map((cacheName) => {
                if (!cacheWhitelist.includes(cacheName)) {
                    return caches.delete(cacheName);
                }
            })
        ))
    );
});
